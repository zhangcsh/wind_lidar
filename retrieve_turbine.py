# Wind lidar data process and analysis
# Author zhangcsh @EMGO
# Data from 
import numpy as np
from numpy import sin,cos,arctan2,arctan,arcsin,tan
import csv
import re
import os
import math
import matplotlib
from datetime import datetime as dt
from datetime import timedelta as td
from matplotlib import pyplot as plt
from sklearn.metrics import r2_score
from matplotlib import dates as mdates
matplotlib.rcParams['axes.unicode_minus']=False
plt.rcParams['font.sans-serif']=['simHei'] #显示中文

# rotation matrix
def rotation(axis, angle):
    s = sin(angle)
    c = cos(angle)
    if axis == 1:
        rows = ((1,  0,  0),
                (0,  c,  s),
                (0, -s,  c))
    elif axis == 2:
        rows = ((c,  0, -s),
                (0,  1,  0),
                (s, 0,  c))
    else:
        rows = ((+c,  s,  0),
                (-s,  c,  0),
                (0,  0,  1))
    return np.double(rows)

# 读取径向数据@zhangqi
def load_radial_vel(file_name):
    #data = np.loadtxt(open(file_name,"rb"),skiprows=1,usecols=[2,3])
    #print(data)
    #beam_flag = []
    #data = []
    radial_vel1,time1,distance1 = [], [], []
    radial_vel2,time2,distance2 = [], [], []
    radial_vel3,time3,distance3 = [], [], []
    radial_vel4,time4,distance4 = [], [], []
    snr1,snr2,snr3,snr4 = [], [], [], []
    if isinstance(file_name, str):    
        #print(file_name)
        f0 = open(fold_dir + file_name,'r')
        csv_reader = csv.reader(f0)
        #print(csv_reader)
        for row in csv_reader:
                    
            #print(type(row[1]))
            try:            
            #if isinstance(row[1],str):
                #print(row[1])
                #continue
            #else:
                #for distance in distances:
                    if abs(float(row[1])- distance) <= 3:
                        #print(row[1])
                    #if row[1] == "100" or row[1] == "99" or row[1] == "101":
                        if row[9] == "1":
                            distance1.append(row[1])
                            radial_vel1.append(float(row[2]))
                            snr1.append(float(row[4]))
                            time1.append(row[7])
                        if row[9] == "2":
                            distance2.append(row[1])
                            radial_vel2.append(float(row[2]))
                            snr2.append(float(row[4]))
                            time2.append(row[7])
                        if row[9] == "3":
                            distance3.append(row[1])
                            radial_vel3.append(float(row[2]))
                            snr3.append(float(row[4]))
                            time3.append(row[7])
                        if row[9] == "4":
                            distance4.append(row[1])
                            radial_vel4.append(float(row[2]))
                            snr4.append(float(row[4]))
                            time4.append(row[7])
            except: continue
    else:
        if len(file_name)>1:
            for file_nam in file_name:
                f0 = open(fold_dir + file_nam,'r')
                csv_reader = csv.reader(f0)
                for row in csv_reader:
                    #print(type(row[1]))
                    try:            
                    #if isinstance(row[1],str):
                        #print(row[1])
                        #continue
                    #else:
                        #for distance in distances:
                            if abs(float(row[1])- distance) <= 3:
                                #print(row[1])
                            #if row[1] == "100" or row[1] == "99" or row[1] == "101":
                                if row[9] == "1":
                                    distance1.append(row[1])
                                    radial_vel1.append(float(row[2]))
                                    snr1.append(float(row[4]))
                                    time1.append(row[7])
                                if row[9] == "2":
                                    distance2.append(row[1])
                                    radial_vel2.append(float(row[2]))
                                    snr2.append(float(row[4]))
                                    time2.append(row[7])
                                if row[9] == "3":
                                    distance3.append(row[1])
                                    radial_vel3.append(float(row[2]))
                                    snr3.append(float(row[4]))
                                    time3.append(row[7])
                                if row[9] == "4":
                                    distance4.append(row[1])
                                    radial_vel4.append(float(row[2]))
                                    snr4.append(float(row[4]))
                                    time4.append(row[7])
                    except: continue
    return distance1,radial_vel1,time1,\
    distance2,radial_vel2,time2,\
    distance3,radial_vel3,time3,\
    distance4,radial_vel4,time4,\
    snr1,snr2,snr3,snr4

# 四束脉冲解算三维风场
def cal_vel4(v1,v2,v3,v4,compass_data=0,delta_alpha=0,delta_theta =0):
    #v1,v2,v3,v4 = load_radial_vel()
    #angleAlf = 30.0 / 2 #上下
    #angleSt = 42.942 / 2  # 1波束与z轴的夹角
    alpha = np.deg2rad(30.0 / 2+delta_alpha)
    theta = np.deg2rad(42.942 / 2+delta_theta)
    half_beta = np.arccos(cos(theta)/cos(alpha))
    #half_beta = np.deg2rad(15.54)
    #half_beta = np.deg2rad(15)
    # 东
    vy = (v1-v2+v4-v3)/(4*np.sin(half_beta))
    # 北
    vx = (v1-v4+v2-v3)/(4*np.cos(theta)*np.tan(alpha))
    # 天
    vz = (v1+v2+v3+v4)/(4*np.cos(theta))
    v_horizontal = np.sqrt(vy**2+vz**2)
    direction = np.rad2deg(arctan2(vy,vz))+compass_data
    if direction<0:
        direction+=360
    if direction>360:
        direction-=360
    return v_horizontal,direction,vx,vy,vz

def fun_retrieve(v1,v2,v3,theta1,theta2,theta3,phi1,phi2,phi3,delta_phi):
    a,b,c = np.linalg.inv(rotation(2,np.deg2rad(delta_phi)))@\
        np.array([cos(theta1)*cos(phi1), sin(theta1)*cos(phi1), sin(phi1)])
    d,e,f = np.linalg.inv(rotation(2,np.deg2rad(delta_phi)))@\
        np.array([cos(theta2)*cos(phi2), sin(theta2)*cos(phi2), sin(phi2)])
    g,h,i = np.linalg.inv(rotation(2,np.deg2rad(delta_phi)))@\
        np.array([cos(theta3)*cos(phi3), sin(theta3)*cos(phi3), sin(phi3)])
    #print(theta1-arctan2(b,a))
    theta10 = arctan2(b,a)
    phi10 = arcsin(c)
    theta20 = arctan2(e,d)
    phi20 = arcsin(f)
    theta30 = arctan2(h,g)
    phi30 = arcsin(i)
    v,u,w = np.linalg.inv(np.array([[cos(theta10)*cos(phi10), sin(theta10)*cos(phi10), sin(phi10)],
                           [cos(theta20)*cos(phi20), sin(theta20)*cos(phi20), sin(phi20)],
                           [cos(theta30)*cos(phi30), sin(theta30)*cos(phi30), sin(phi30)]]))@np.array([v1,v2,v3])
    #u = ((v1-v2)*(sin(theta3)-sin(theta1))-(v1-v3)*(sin(theta2)-sin(theta1)))/(cos(phi)*(sin(theta1-theta2)+sin(theta2-theta3)+sin(theta3-theta1)))
    #v = ((v1-v2)*(cos(theta1)-cos(theta3))-(v1-v3)*(cos(theta1)-cos(theta2)))/(cos(phi)*(sin(theta1-theta2)+sin(theta2-theta3)+sin(theta3-theta1)))
    #w = (v1*sin(theta2-theta3)+v2*sin(theta3-theta1)+v3*sin(theta1-theta2))/sin(phi)*(sin(theta1-theta2)+sin(theta2-theta3)+sin(theta3-theta1))
    #u = ((v1-v2)*(cos(theta1)-cos(theta3))-(v1-v3)*(cos(theta1)-cos(theta2)))/(cos(phi)*(sin(theta2-theta3)+sin(theta3-theta1)+sin(theta1-theta2)))
    #v = ((v1-v2)*(sin(theta1)-sin(theta3))-(v1-v3)*(sin(theta1)-sin(theta2)))/(cos(phi)*(sin(theta3-theta2)+sin(theta2-theta1)+sin(theta1-theta3)))
    #w = (((v1*sin(theta2)-v2*sin(theta1))*sin(theta3-theta1))-(v1*sin(theta3)-v3*sin(theta1)*sin(theta2-theta1)))/(sin(phi)*((sin(theta2)-sin(theta1))*sin(theta3-theta1)-(sin(theta3)-sin(theta1))*sin(theta2-theta1)))
    #print(theta1,theta2,theta3,phi)
    return u,v,w

# 三束脉冲解算三维风场
def cal_vel3_v1(v1,v2,v3,v4,compass_data=0,delta_theta1=0,delta_theta2=0,delta_theta3=0):
    alpha = np.deg2rad(7.5)
    theta = np.deg2rad(12.44)
    half_beta = np.arccos(cos(theta)/cos(alpha))
    #print(np.rad2deg(half_beta))
    #alpha2 = np.deg2rad(-7.5)
    #theta2 = np.deg2rad(-14.1)
    #half_beta2 = np.arccos(cos(theta2)/cos(alpha2))

    #print(np.rad2deg(half_beta))
    #half_beta = np.deg2rad(15.54)
    delta_phi = -5 #degree
    x,y,z = cos(theta)*tan(alpha),np.sqrt(cos(alpha)**2-cos(theta)**2)/cos(alpha),cos(theta)
    theta0 = arctan2(y,z)
    phi0 = arcsin(x)
    #print(np.rad2deg(theta0),np.rad2deg(phi0))
    theta = np.array([theta0,-theta0,-theta0,theta0])
    phi = np.array([phi0,phi0,-phi0,-phi0])
    #如果第一径向数值无效，用2，3，4光束数据
    #delta_phi = 0
    if v1 == -1000:
        v1,v2,v3 = v2,v3,v4
        #2
        #theta1 = 2*np.pi-np.arcsin(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta1)
        #3
        #theta2 = np.pi+np.arcsin(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta2)
        #4
        #theta3 = np.pi/2+np.arccos(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta3)
        theta1,theta2,theta3=theta[1],theta[2],theta[3]
        phi1,phi2,phi3=phi[1],phi[2],phi[3]
        u,v,w = fun_retrieve(v1,v2,v3,theta1,theta2,theta3,phi1,phi2,phi3,delta_phi)

    #如果第2径向数值无效，用1，3，4光束数据
    elif v2 == -1000:
        v1,v2,v3 = v1,v3,v4
        #1
        #theta1 = np.arcsin(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta1)
        #3
        #theta2 = np.pi+np.arcsin(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta2)
        #4
        #theta3 = np.pi/2+np.arccos(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta3)
        
        theta1,theta2,theta3=theta[0],theta[2],theta[3]
        phi1,phi2,phi3=phi[0],phi[2],phi[3]
        u,v,w = fun_retrieve(v1,v2,v3,theta1,theta2,theta3,phi1,phi2,phi3,delta_phi)
    
    elif v3 == -1000:
        v1,v2,v3 = v1,v2,v4
        #1
        #theta1 = np.arcsin(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta1)
        #2
        #theta2 = 2*np.pi-np.arcsin(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta2)
        #4
        #theta3 = np.pi/2+np.arcsin(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta3)
        theta1,theta2,theta3=theta[0],theta[1],theta[3]
        phi1,phi2,phi3=phi[0],phi[1],phi[3]
        u,v,w = fun_retrieve(v1,v2,v3,theta1,theta2,theta3,phi1,phi2,phi3,delta_phi)
    
    elif v4 == -1000:
        v1,v2,v3 = v1,v2,v3
        #1
        #theta1 = np.arcsin(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta1)
        #2
        #theta2 = 2*np.pi-np.arcsin(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta2)
        #3
        #theta3 = np.pi+np.arcsin(np.sqrt(cos(alpha)**2-cos(theta)**2)/(sin(theta)*cos(alpha)))+np.deg2rad(delta_theta3)
        theta1,theta2,theta3=theta[0],theta[1],theta[2]
        phi1,phi2,phi3=phi[0],phi[1],phi[2]
        u,v,w = fun_retrieve(v1,v2,v3,theta1,theta2,theta3,phi1,phi2,phi3,delta_phi)
    v_horizontal = np.sqrt(u**2+v**2)
    #print(u,v,w)
    #print(np.rad2deg(theta1),np.rad2deg(theta2),np.rad2deg(theta3))
    direction = np.rad2deg(arctan(u/v))#+compass_data
    return v_horizontal,direction,u,v,w

def cal_vel(v1,v2):
    alpha = np.deg2rad(7.5)
    #7.5
    #14.1
    #5
    theta = np.deg2rad(14.1)
    beta = 2*np.arccos(np.cos(theta)/np.cos(alpha))
    half_beta = beta/2
    x,y,z = cos(theta)*tan(alpha),np.sqrt(cos(alpha)**2-cos(theta)**2)/cos(alpha),cos(theta)
    theta0 = arctan2(y,z)
    phi0 = arcsin(x)
    #print(np.rad2deg(theta0),np.rad2deg(phi0))
    #print(theta0,phi0)
    delta_phi = 3.5
    theta00 = np.array([theta0,-theta0,-theta0,theta0])
    phi00 = np.array([phi0,phi0,-phi0,-phi0])
    theta1,theta2=theta00[0], theta00[1]
    phi1,phi2=phi00[0], phi00[1]
    #print(np.rad2deg(theta0),np.rad2deg(phi0))
    a,b,c = np.linalg.inv(rotation(2,np.deg2rad(delta_phi)))@\
        np.array([cos(theta1)*cos(phi1), sin(theta1)*cos(phi1), sin(phi1)])
    d,e,f = np.linalg.inv(rotation(2,np.deg2rad(delta_phi)))@\
        np.array([cos(theta2)*cos(phi2), sin(theta2)*cos(phi2), sin(phi2)])    
    
    #print(cos(theta1)*cos(phi1),sin(theta1)*cos(phi1))
    #print(cos(theta2)*cos(phi2), sin(theta2)*cos(phi2))
    #print(d,e,f)
    #print([cos(theta1)*cos(phi1), sin(theta1)*cos(phi1), sin(phi1)],a,b,c)
    theta10 = arctan2(b,a)
    phi10 = arcsin(c)
    theta20 = arctan2(e,d)
    phi20 = arcsin(f)
    #print(np.rad2deg(theta10),np.rad2deg(phi10))
    #print(np.rad2deg(theta20),np.rad2deg(phi20))
    #print(np.rad2deg(theta10),np.rad2deg(theta20),np.rad2deg(phi10),np.rad2deg(phi20))
    #[cos(theta10)*cos(phi10), sin(theta10)*cos(phi10), sin(phi10)],\
    #                       [cos(theta20)*cos(phi20), sin(theta20)*cos(phi20), sin(phi20)]
    #print(np.rad2deg(beta))
    m = np.cos(half_beta)*np.sin(alpha)
    n = np.sin(half_beta)
    q = np.cos(half_beta)*np.cos(alpha)
    #x1 = (v2-v3)/(2*n)
    #x2 = (v1-v4)/(2*n)
    y1 = (v1-v2)/(2*sin(theta10)*cos(phi10))
    #y2 = (v4-v3)/(2*m)
    z1 = (v1+v2)/(2*cos(theta20)*cos(phi10))
    #2 = (v2+v4)/(2*q)
    v,u = np.linalg.inv(np.array([[cos(theta10)*cos(phi10), sin(theta10)*cos(phi10)],
                           [cos(theta20)*cos(phi20), sin(theta20)*cos(phi20)]]))@np.array([v1,v2])
    #return np.sqrt(y1**2+z1**2),y1,z1
    return np.sqrt(u**2+v**2),u,v

# 计算标准差
def cal_delta(args,x,y):
    delta_y = []
    y_fit = []
    for xi in x:
        yi = 0
        for i in range(len(args)):
            yi = yi + args[i] * (xi ** (len(args)-(i+1)))
        y_fit.append(yi)
    delta_y = np.array(y) - np.array(y_fit)

    delta = np.std(delta_y)
    return np.array(y_fit),np.array(delta)

def load_ul_turbine(file_name_part="2023-06-06_"):
    tim_ref,temp,dire = [],[],[]
    for file in files:
        if file_name_part in file:
            #print(file)
            f = open(fold_dir + file, 'r')
            lines = f.readlines()
            for l,line in enumerate(lines):
                #print(l)
                if l == 0:
                    stamp = line.split(";")
                    for m,sta in enumerate(stamp):
                        #if 'Timestamp' in sta:
                            #pos_time = m
                        if "WS1_035_Mean" in sta:
                            pos_200m = m
                        if "WV1_035_Mean" in sta:                                
                            pos_200m_deg = m
                        #if "{}.00m_N_Avg_Deg".format(int(distance)) in sta:                                
                        #    pos_200m_deg = m+1
                    #print(stamp)
            #print(pos_200m)
            for l,line in enumerate(lines):
                if (l > 0):
                    s = line.split(";")
                    pos_time = 0
                    #ymd = s[pos_time].split("-")
                    ymd = s[pos_time].split("/")
                    year = int(ymd[2])
                    month = int(ymd[0])
                    date = int(ymd[1])
                    hms = s[pos_time+1].split(":")
                    hour = int(hms[0])
                    minute = int(hms[1])
                    #second = int(hms[2])
                    second = 0
                    #print(ymd,hms)
                    tim_ref.append(dt(year,month,date,hour,minute,second))
                    temp.append(float(s[pos_200m]))
                    dire.append(float(s[pos_200m_deg]))
    return tim_ref,temp,dire

if __name__=='__main__':
    #i=0                    
    #for delta_alpha in np.arange(-2,3,1):
        #for delta_theta in np.arange(-2,3,1):                            
            '''
            delta_theta1 = 0
            delta_theta2 = -2
            delta_theta3 = 2
            delta_phi = -1
            2.       -2.        2.
            '''
            # 角度修正量为0
            delta_alpha = 0
            delta_theta = 0
            delta_theta1 = 0
            delta_theta2 = 0
            delta_theta3 = 0
            delta_phi = 0
            min_snr = 0.25
            max_snr = 5
            #direction = []
            # 目录名
            fold_dir = os.getcwd()+'/机舱_UL/'
            #fold_dir = os.getcwd()+'/机舱_鉴衡/'
            files = os.listdir(fold_dir)
            #print(fold_dir)
            #i+=1
            #out.write("ymd ref det\n")
            # 输入文档名，读取测量数据

            '''
            file_name = np.array(['jx8.15-17.csv','jx8.18-20.csv'])           
            file_name = []
            for file in files:
                if "Mast" in file:
                    continue
                elif bool(re.search(r'\d', file)):
                    if ('comp' not in file):
                #elif "csv" in file:
                        file_name.append(file)   
                        '''            
            file_name = 'jx9.1-11_200.csv'

            #需要对比的高度层
            distances = np.arange(200,220,20)
            for distance in distances:
                temp_gh = []
                direction_gh = []
                tim_gh = []
                out = open(fold_dir+"comp{}.csv".format(int(distance)),'w')
                # 读取时间、风速和风向
                tim_ref,temp,dire = load_ul_turbine()
                #print(tim_ref,temp,dire)
                #print(tim_ref)
                v_hori = []
                v_dire = []
                t = []
                v_u,v_v = [], []
                compass_data = 337-180
                distance1,radial_vel1,time1,\
                    distance2,radial_vel2,time2,\
                    distance3,radial_vel3,time3,\
                    distance4,radial_vel4,time4,\
                    snr1,snr2,snr3,snr4 = load_radial_vel(file_name)
                #print(distance1)
                for l,tim in enumerate(time1):
                    try:
                        # 四波束径向风速
                        v1,v2,v3,v4 = radial_vel1[l],radial_vel2[l],radial_vel3[l],radial_vel4[l]
                        sn1,sn2,sn3,sn4 = snr1[l],snr2[l],snr3[l],snr4[l]
                        v1_j = (v1 != -1000)
                        v2_j = (v2 != -1000)
                        v3_j = (v3 != -1000)
                        v4_j = (v4 != -1000)
                        v_all_j = (v1_j+v2_j+v3_j+v4_j)
                        v_all_jj = (v1_j+v2_j)
                        '''
                        if v_all_j == 4:
                            if (min_snr < sn1 and sn1 < max_snr) and (min_snr < sn2 and sn2 < max_snr)\
                                and (min_snr < sn4 and sn4 < max_snr):
                                v_horizontal,direction,u,v,w = cal_vel4(v1,v2,v3,v4,compass_data,delta_alpha,delta_theta )
                                ymd = re.split('/| |:',tim)
                                yr = int(ymd[2])
                                month = int(ymd[1])
                                date = int(ymd[0])
                                hour = int(ymd[3])
                                minute = int(ymd[4])
                                second = int(ymd[5])
                                t.append(dt(yr,month,date,hour,minute,second))
                                v_hori.append(v_horizontal)
                                v_u.append(u)
                                v_v.append(v)
                                v_dire.append(direction)        
                                '''
                        if v_all_j == 3:
                            #print(True)
                            if (min_snr < sn1 and sn1 < max_snr) and (min_snr < sn2 and sn2 < max_snr)\
                                and (min_snr < sn4 and sn4 < max_snr):
                                v_horizontal,direction,u,v,w = cal_vel3_v1(v1,v2,v3,v4,compass_data,delta_alpha,delta_theta)
                                ymd = re.split('/| |:',tim)
                                yr = int(ymd[2])
                                month = int(ymd[1])
                                date = int(ymd[0])
                                hour = int(ymd[3])
                                minute = int(ymd[4])
                                second = int(ymd[5])
                                t.append(dt(yr,month,date,hour,minute,second))
                                v_hori.append(v_horizontal)
                                v_u.append(u)
                                v_v.append(v)                        
                        else:
                            # 上两波束正常工作
                            if v_all_jj == 2:
                            #if (min_snr < sn1 and sn1 < max_snr) and (min_snr < sn2 and sn2 < max_snr)\
                            #    and (min_snr < sn4 and sn4 < max_snr):
                                v_horizontal,u,v = cal_vel(v1,v2)
                                ymd = re.split('/| |:',tim)
                                #print(ymd)
                                yr = int(ymd[2])
                                month = int(ymd[1])
                                date = int(ymd[0])
                                hour = int(ymd[3])
                                minute = int(ymd[4])
                                second = int(ymd[5])
                                if v_horizontal < 60:
                                    t.append(dt(yr,month,date,hour,minute,second))
                                    v_hori.append(v_horizontal)     
                                    v_u.append(u)
                                    v_v.append(v)
                    except:        continue
                #print(t)
                t_ave,v_ave = [], []
                d_ave = []
                min_t = np.min(t)
                max_t = np.max(t)
                delta_month = int(max_t.month) - int(min_t.month)
                delta_day = int(max_t.day) - int(min_t.day)
                delta_hour = int(max_t.hour) - int(min_t.hour)
                delta_minute = int(max_t.minute) - int(min_t.minute)
                dyn_leap = np.array([31, 29, 31, 30, 31, 30, 31, 31, 30, 31 ,30 ,31])
                dyn_norm = np.array([31, 28, 31, 30, 31, 30, 31, 31, 30, 31 ,30 ,31])
                del_date = 0
                if delta_month == 0:
                    len_t = int((delta_day * 24 * 60 + delta_hour * 60 + delta_minute)/10)
                else:
                    if np.mod(max_t.year,4) == 0:
                        for jj in range(int(min_t.month),int(max_t.month)):
                            del_date += dyn_leap[jj-1]
                        len_t = int(((del_date+ delta_day) * 24 * 60 + delta_hour * 60 + delta_minute)/10)
                    else:
                        for jj in range(int(min_t.month),int(max_t.month)):
                            del_date += dyn_norm[jj-1]
                        len_t = int(((del_date + delta_day) * 24 * 60 + delta_hour * 60 + delta_minute)/10)
                #print(len_t)
                for i in range(0,len_t):
                    #print(i)
                    v_range = []
                    v_v_range = []
                    v_u_range = []
                    tt_v = []
                    # 每十分钟的数据保存到一个数组里
                    for l,tt in enumerate(t):
                        if ((tt > (dt(min_t.year,min_t.month,min_t.day) + td(minutes=i*10))) and\
                            (tt <= (dt(min_t.year,min_t.month,min_t.day) + td(minutes=i*10+10)))):
                            v_range.append(v_hori[l])
                            tt_v.append(tt)
                            v_v_range.append(v_v[l])
                            v_u_range.append(v_u[l])
                            #print(tt,v_hori[l])
                    #print(v_range)
                    # 判断每个数组是否为空
                    if np.isnan(np.average(v_range)):
                        #print(v_ave)
                        continue
                    else:
                        if len(v_range) > 5:
                            v_range_pr = []
                            tt_v1 = []
                            v_v_pr = []
                            v_u_pr = []
                            t_ave.append(dt(min_t.year,min_t.month,min_t.day) + td(minutes=i*10))
                            k, b = np.polyfit(np.arange(len(v_range)),v_range,1)
                            y_fit,delta = cal_delta(np.array([k,b]),np.arange(len(v_range)),v_range)
                            #print(k, b)
                            for l,v_ra in enumerate(v_range):
                                #print(v_ra,y_fit[l])
                                # 3σ剔除粗大误差
                                if abs(v_ra-y_fit[l])<=3*delta:                                            
                                    v_range_pr.append(v_ra)
                                    tt_v1.append(tt_v[l])
                                    v_v_pr.append(v_v_range[l])
                                    v_u_pr.append(v_u_range[l])
                            
                            #print(v_u_pr)
                            # 计算每十分钟测量数据的平均值
                            v_ave.append(np.average(v_range_pr))
                            d_av = np.rad2deg(arctan2(np.average(v_u_pr),np.average(v_v_pr)))+compass_data
                            if d_av > 360:
                                d_av -= 360
                            if d_av < 0:
                                d_av += 360    
                            d_ave.append(d_av)
                            #print(delta, len(v_range_pr)/len(v_range))
                            '''

                            plt.plot(tt_v,y_fit,'-',markersize=8,label='拟合数据')
                            plt.plot(tt_v1,v_range_pr,'o',markersize=8,label='1$\sigma$剔除后')
                            plt.plot(tt_v,v_range,'.',markersize=8,label='原始数据')
                            plt.legend()
                            ax = plt.gca()
                            ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
                            plt.xlabel('时间',fontsize = 30)
                            plt.ylabel('风速 [m/s]',fontsize = 30)
                            plt.tick_params(labelsize=20)
                            plt.show()
                            '''
                        #print(dt(yr,month,date) + td(minutes=i*10),np.average(v_range))
                #print(d_ave)
                eff,eff_gh,eff_poly = [], [], []
                eff_d, eff_gh_d = [], []
                #print(t_ave)    
                for l,tim_gh0 in enumerate(t_ave):
                    for n,tim0 in enumerate(tim_ref):
                        if (tim_gh0 == tim0):
                            out.write("%s %.3f %1.3f %1.3f %.1f %.1f %.1f\n" % \
                                        (tim_gh0,temp[n],v_ave[l],temp[n]-v_ave[l],dire[n],d_ave[l],dire[n]-d_ave[l]))
                            eff.append(temp[n])
                            eff_gh.append(v_ave[l])
                            eff_d.append(dire[n])
                            eff_gh_d.append(d_ave[l])
                
                #print(eff,eff_gh)
                #print(np.polyfit(eff,eff_gh,1))
                # 拟合测风塔 vs 测风雷达一次函数
                k,b = np.polyfit(eff,eff_gh,1)
                #print(k,b,delta_theta1,delta_theta2,delta_theta3,delta_phi,r2_score(eff,eff_gh))
                #output_f.write("%.3f %1.3f %.3f %.3f %.3f %.3f %.6f \n" % \
                #                       (k,b,delta_theta1,delta_theta2,delta_theta3,delta_phi,r2_score(eff,eff_gh)))                    
                
                for i in eff:
                    eff_poly.append(i)
                #arg1,arg2 = np.polyfit(eff,eff_gh,1)
                #print(arg1,arg2) 
                eff1,eff_gh1 = [], []
                eff_gh_d1 = []
                eff_d1 = []
                #arg1,arg2 = 1,0
                # 计算标准差
                y_fit,delta = cal_delta(np.array([k,b]),eff,eff_gh)
                # 利用风塔
                #args = np.array([-0.01082971, 1.06997638, -0.42777182])
                args = np.array([-0.00082971, 1.06997638, -0.07])
                for l,res in enumerate(eff_gh):
                    if delta > 1:
                        if abs(res-y_fit[l]) < 3 * 1:
                            eff1.append(eff[l])
                            #eff_gh1.append((-args[1]+np.sqrt(args[1]**2-4*args[0]*(args[2]-res)))/(2*args[0]))
                            eff_gh1.append((res-0.543)/1.16)
                            #eff_gh1.append(res)
                            if abs(eff_gh_d[l]-eff_d[l])<180:
                                eff_gh_d1.append(eff_gh_d[l])
                                eff_d1.append(eff_d[l])
                    else:
                        if abs(res-y_fit[l]) < 3 * delta:
                            #out.write("%s %.3f %1.3f \n" % \
                            #(tim_gh0,eff[l],res))
                            eff1.append(eff[l])
                            eff_gh1.append((res-0.543)/1.16)
                            #eff_gh1.append(res)
                            if abs(eff_gh_d[l]-eff_d[l])<180:
                                eff_gh_d1.append(eff_gh_d[l])
                                eff_d1.append(eff_d[l])
                            #eff_gh1.append((-args[1]+np.sqrt(args[1]**2-4*args[0]*(args[2]-res)))/(2*args[0]))
                y_fit1,delta1 = cal_delta(np.array([k,b]),eff1,eff_gh1)
                print(np.polyfit(eff1,eff_gh1,1),delta1)
                print(r2_score(eff1,eff_gh1))
                k1,b1 = np.polyfit(eff_d1,eff_gh_d1,1)
                y_fit2,delta2 = cal_delta(np.array([k,b]),eff_d1,eff_gh_d1)
                print(np.polyfit(eff_d1,eff_gh_d1,1),delta2)
                print(r2_score(eff_d1,eff_gh_d1))
                fig = plt.figure()
                ax = fig.add_subplot(111)
                plt.plot(eff_d1,eff_gh_d1,'.',markersize=8)
                plt.plot(eff_d1,eff_d1,'-',markersize=8)
                plt.xlabel('Mast Wind Direction $@$ {}m [m/s]'.format(distance),fontsize = 30)
                plt.ylabel('RSD Wind Direction $@$ {}m [m/s]'.format(distance),fontsize = 30)                
                plt.tick_params(labelsize=20)
                ax2 = ax.twinx() 
                ax2.plot(eff_d1,(np.array(eff_gh_d1)-np.array(eff_d1)),'.r',markersize=8)
                ax2.set_ylabel('Deviation [degree]',fontsize=30)
                ax2.set_ylim(-100,100)
                plt.tick_params(labelsize=20)
                plt.tick_params(axis='y',colors='red')
                plt.title('{}m水平风向数据对比'.format(int(distance)),fontsize = 30)
                # 图1 
                fig = plt.figure()
                ax = fig.add_subplot(111)
                plt.plot(eff1,eff_gh1,'.',markersize=8)
                plt.plot(eff,eff_poly,'-',markersize=8)
                #plt.plot(tim_gh,temp_gh,'.',markersize=8)
                #plt.plot(t_ave, v_ave,'.',markersize=8)
                #plt.legend(["参考值","后处理"],fontsize = 30)
                #plt.legend(["参考值","测量值","后处理"],fontsize = 30)
                plt.xlabel('Mast Wind Speed $@$ {}m [m/s]'.format(distance),fontsize = 30)
                plt.ylabel('RSD Wind Speed $@$ {}m [m/s]'.format(distance),fontsize = 30)
                plt.tick_params(labelsize=20)
                
                ax2 = ax.twinx() 
                ax2.plot(eff1,(np.array(eff_gh1)-np.array(eff1))/eff1*100,'.r',markersize=8)
                ax2.set_ylabel('Deviation [$\%$]',fontsize=30)
                ax2.set_ylim(-10,10)
                plt.tick_params(labelsize=20)
                plt.tick_params(axis='y',colors='red')
                plt.title('{}m水平风速数据对比'.format(int(distance)),fontsize = 30)
                eff3_gh3_mean,eff3_mean,eff3_gh3_std,eff3_gh3_std_ = [], [], [], []
                for i in range(math.ceil((np.max(eff_gh1)-np.min(eff_gh1))/0.5)):
                    eff_gh3, eff3 = [], []
                    for l, eff_gh2 in enumerate(eff_gh1):
                        if ((eff_gh2>(3.25+i*0.5)) and (eff_gh2<(3.25+(i+1)*0.5))):
                            eff_gh3.append(eff_gh2)
                            eff3.append(eff1[l])
                    if len(eff_gh3)>=5:
                        eff3_gh3_mean.append(np.mean(eff_gh3))
                        eff3_gh3_std.append(np.std(eff_gh3)/np.mean(eff_gh3)*100)
                        eff3_gh3_std_.append(-np.std(eff_gh3)/np.mean(eff_gh3)*100)
                        eff3_mean.append(np.mean(eff3))
                fig = plt.figure()
                ax = fig.add_subplot(111)
                plt.plot(eff3_mean,eff3_gh3_mean,'.',markersize=8)
                plt.plot(eff,eff_poly,'-',markersize=8)
                #k1,b1 = np.polyfit(eff3_mean,eff3_gh3_mean,1)
                #y_fit,delta = cal_delta(np.array([k1,b1]),eff3_mean,eff3_gh3_mean)
                #print(delta)
                #plt.plot(tim_gh,temp_gh,'.',markersize=8)
                #plt.plot(t_ave, v_ave,'.',markersize=8)
                #plt.legend(["参考值","后处理"],fontsize = 30)
                #plt.legend(["参考值","测量值","后处理"],fontsize = 30)
                plt.xlabel('Mast Wind Speed $@$ {}m [m/s]'.format(distance),fontsize = 30)
                plt.ylabel('RSD Wind Speed $@$ {}m [m/s]'.format(distance),fontsize = 30)                        
                plt.tick_params(labelsize=20)                          
                ax2 = ax.twinx()
                plt.plot(eff3_mean,eff3_gh3_std,'-k',markersize=8)
                plt.plot(eff3_mean,eff3_gh3_std_,'-k',markersize=8)
                ax2.plot(eff3_mean,(np.array(eff3_gh3_mean)-np.array(eff3_mean))/eff3_mean*1e2,'.r',markersize=8)
                ax2.set_ylabel('Deviation [$\%$]',fontsize=30)
                ax2.set_ylim(-10,10)
                plt.tick_params(labelsize=20)
                plt.tick_params(axis='y',colors='red')
                plt.title('{}m水平风速数据对比'.format(int(distance)),fontsize = 30)
                '''
                plt.figure()
                plt.plot(eff3_mean,(np.array(eff3_gh3_mean)-np.array(eff3_mean))/eff3_mean*1e2,'o-',markersize=8)
                plt.xlabel('Mast Wind Speed $\@$ {}m [m/s]'.format(distance),fontsize = 30)
                plt.ylabel('Deviation [$\%$]',fontsize = 30)
                plt.tick_params(labelsize=20)
                plt.title('{}m水平风速数据对比'.format(int(distance)),fontsize = 30)
                '''
plt.show()
                        #plt.close()
                        #plt.savefig("figs/fig_{}_{}.eps".format(delta_alpha,delta_theta)) 
    #except:continue
        #plt.legend(["参考值","后处理"],fontsize = 30)
        #plt.legend(["参考值","测量值","后处理"],fontsize = 30)
        #plt.xlabel('参考风速 [m/s]',fontsize = 30)
        #plt.ylabel('测量风速 [m/s]',fontsize = 30)
        #plt.tick_params(labelsize=20)
        #plt.title('{}m水平风速数据对比'.format(int(distance)),fontsize = 30)
        #plt.figure()
        #plt.plot(eff,eff_gh,'.',markersize=8)
#print(np.polyfit(eff,eff_gh,1))
#print(r2_score(eff,eff_gh))
#plt.show()
#print(np.std(np.array(eff)-np.array(eff_gh)))